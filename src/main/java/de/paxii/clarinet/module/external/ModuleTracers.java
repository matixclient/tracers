package de.paxii.clarinet.module.external;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.event.EventHandler;
import de.paxii.clarinet.event.events.game.RenderTickEvent;
import de.paxii.clarinet.function.ThrowingStringFunction;
import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;
import de.paxii.clarinet.util.render.GL11Helper;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.VertexBuffer;
import net.minecraft.client.renderer.EntityRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.Vec3d;

import org.lwjgl.opengl.GL11;

import java.awt.*;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * Created by Lars on 22.03.2016.
 */
public class ModuleTracers extends Module {
  public ModuleTracers() {
    super("Tracers", ModuleCategory.RENDER);

    this.setVersion("2.0");
    this.setBuildVersion(16000);
    this.setRegistered(true);
  }

  @EventHandler
  public void onGlobalRender(RenderTickEvent renderTickEvent) {
    try {
      this.setup(true);
      this.renderTracers(renderTickEvent.getRenderPartialTicks());
      this.setup(false);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Re-orient the camera and render tracers for all players
   */
  private void renderTracers(float partialTicks) throws Exception {
    RenderManager renderManager = Wrapper.getMinecraft().getRenderManager();

    Method orientCamera = EntityRenderer.class.getDeclaredMethod("orientCamera", float.class);
    orientCamera.setAccessible(true);
    orientCamera.invoke(Wrapper.getRenderer(), partialTicks);

    ThrowingStringFunction<Double> renderPosition = (position) -> {
      Field renderPos = RenderManager.class.getDeclaredField("renderPos" + position);
      renderPos.setAccessible(true);
      return renderPos.getDouble(renderManager);
    };
    double[] startingPosition = new double[]{
            renderPosition.apply("X"), renderPosition.apply("Y"), renderPosition.apply("Z")
    };
    GL11.glTranslated(-startingPosition[0], -startingPosition[1], -startingPosition[2]);
    Vec3d startPoint = Wrapper.getPlayer().getLook(partialTicks)
            .addVector(0, Wrapper.getPlayer().getEyeHeight(), 0)
            .addVector(startingPosition[0], startingPosition[1], startingPosition[2]);

    Wrapper.getWorld().getLoadedEntityList().forEach(e -> this.renderTracerForEntity(startPoint, e));
  }

  /**
   * Renders the tracer for a player
   */
  private void renderTracerForEntity(Vec3d startingPoint, Entity renderEntity) {
    if (renderEntity instanceof EntityPlayer && renderEntity != Wrapper.getPlayer()) {
      if (!(Wrapper.getMinecraft().currentScreen instanceof GuiContainer)) {
        double distance = Wrapper.getPlayer().getDistanceToEntity(renderEntity);

        if (distance <= 100.0D) {
          Vec3d endPoint = renderEntity.getEntityBoundingBox().getCenter().subtract(0, 1, 0);
          int lineColor = Wrapper.getFriendManager().getFriendColor(renderEntity.getName());
          this.drawLine(startingPoint, endPoint, new Color(lineColor));
        }
      }
    }
  }

  /**
   * Draws a line
   * FIXME: Move to opengl helper class?
   */
  private void drawLine(Vec3d start, Vec3d end, Color lineColor) {
    Tessellator tessellator = Tessellator.getInstance();
    VertexBuffer vertexbuffer = tessellator.getBuffer();
    vertexbuffer.begin(3, DefaultVertexFormats.POSITION_COLOR);
    vertexbuffer.pos(start.xCoord, start.yCoord, start.zCoord).color(
            lineColor.getRed(),
            lineColor.getGreen(),
            lineColor.getBlue(),
            lineColor.getAlpha()
    ).endVertex();
    vertexbuffer.pos(end.xCoord, end.yCoord, end.zCoord).color(
            lineColor.getRed(),
            lineColor.getGreen(),
            lineColor.getBlue(),
            lineColor.getAlpha()
    ).endVertex();
    tessellator.draw();
  }

  /**
   * Setup opengl constants. This loads the identity matrix to reset view bobbing
   */
  private void setup(boolean enable) {
    if (enable) {
      GL11.glPushMatrix();
      GL11.glLoadIdentity();
      GL11Helper.enableDefaults();
      GL11.glEnable(GL11.GL_LINE_SMOOTH);
      GL11.glEnable(GL11.GL_BLEND);
      GL11.glDisable(GL11.GL_DEPTH_TEST);
      GL11.glDisable(GL11.GL_LIGHTING);
      GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
      GL11.glDepthMask(false);
      GlStateManager.depthMask(false);
      GlStateManager.disableTexture2D();
      GlStateManager.disableLighting();
      GlStateManager.disableCull();
      GlStateManager.disableBlend();
      GL11.glLineWidth(1.0F);
    } else {
      GL11Helper.disableDefaults();
      GlStateManager.enableTexture2D();
      GlStateManager.enableLighting();
      GlStateManager.enableCull();
      GlStateManager.disableBlend();
      GlStateManager.depthMask(true);
      GL11.glDepthMask(true);
      GL11.glEnable(GL11.GL_DEPTH_TEST);
      GL11.glDisable(GL11.GL_BLEND);
      GL11.glEnable(GL11.GL_LIGHTING);
      GL11.glDisable(GL11.GL_LINE_SMOOTH);
      GL11.glPopMatrix();
    }
  }
}
